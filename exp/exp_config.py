#!/usr/bin/env python


# the ip to be attacked
default_target_ip = 'rhg4'

ports_mapping = {
'web_1.py': '8001',
'web_2.py': '8002',
'web_3.py': '8003',
'web_4.py': '8004',
'web_5.py': '8005',
'web_6.py': '8006',
'web_7.py': '8007',
'web_8.py': '8008',
'web_9.py': '8009',
'web_10.py': '8010',
'server_1.py':'9001',
'server_2.py':'9002',
'server_3.py':'9003',
'server_4.py':'9004',
'server_5.py':'9005',
'server_6.py':'9006',
'server_7.py':'9007',
'server_8.py':'9008',
'server_9.py':'9009',
'server_10.py':'9010'
}

ssh_mapping = {
'web_1.py': '2201',
'web_2.py': '2202',
'web_3.py': '2203',
'web_4.py': '2204',
'web_5.py': '2205',
'web_6.py': '2206',
'web_7.py': '2207',
'web_8.py': '2208',
'web_9.py': '2209',
'web_10.py': '2210',
'server_1.py':'2301',
'server_2.py':'2302',
'server_3.py':'2303',
'server_4.py':'2304',
'server_5.py':'2305',
'server_6.py':'2306',
'server_7.py':'2307',
'server_8.py':'2308',
'server_9.py':'2309',
'server_10.py':'2310'
}



