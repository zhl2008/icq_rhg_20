#!/usr/bin/env python2

import os
import sys
import time
import re
import tornado.httpserver
import tornado.web
import tornado.ioloop

class MainHandler(tornado.web.RequestHandler):

    def get(self):
        self.write('haozigege\n')

    def post(self):
        flag = self.request.files['flag'][0]['body'].strip()
        print(flag)
        open('./flag_9','w').write(flag.decode("utf-8"))
        self.write(flag)

        self.finish()
        tornado.ioloop.IOLoop.instance().stop()



if __name__ == '__main__':
    print("[+] Starting the web server on ports 4443\n")
    application = tornado.web.Application([
        (r'/.*', MainHandler)
    ])
    http_server = tornado.httpserver.HTTPServer(
        application,
        ssl_options = {
            "certfile": os.path.join("./", "server.cert"),
            "keyfile": os.path.join("./", "server.key"),
        }
    )
    http_server.listen(4443)
    tornado.ioloop.IOLoop.current().start()
