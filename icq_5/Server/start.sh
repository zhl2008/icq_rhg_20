#!/bin/bash

# change ssh passwd
echo root:root | chpasswd

# start services
service ssh start

# copy flag
cp /root/run/flag /tmp/flag

# run the main program
/usr/local/tomcat/bin/startup.sh
# thread block
tail -f /dev/null
