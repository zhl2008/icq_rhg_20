#!/bin/bash

/root/clean.sh
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
chmod 777 -R /var/www/html
chown www-data:www-data -R /var/www/html
service apache2 start
service mysql start
mysql -uroot -proot < /root/dz.sql
service ssh start
cp /root/flag /tmp/flag
tail -f /dev/null
