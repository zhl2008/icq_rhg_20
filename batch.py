#!/usr/bin/env python

import os
import sys

raw_compose_dirs = ['1','2','3','4','5','6','7','8','9','10']
compose_dirs = ['icq_' + x for x in raw_compose_dirs]
web_containers = ['Web_' + x for x in raw_compose_dirs]
server_containers = ['Server_' + x for x in raw_compose_dirs]
web_exploits = ['web_' + x for x in raw_compose_dirs]
server_exploits = ['server_' + x for x in raw_compose_dirs]
check_scripts = ['check_' + x for x in raw_compose_dirs]


def start():
    for compose_dir in compose_dirs:
	os.chdir(compose_dir)
	os.system('docker-compose up -d')
	os.chdir('../')

def compile():
    for compose_dir in compose_dirs:
	os.chdir(compose_dir)
	os.system('docker-compose build')
	os.chdir('../')

def init():
    for compose_dir in compose_dirs:
	os.chdir('basic_docker')
	os.system('./build_basic.sh')
	os.chdir("../")

def exploit():
    os.chdir('new_exp')
    for web_exploit in web_exploits:
        os.system('python2 %s.py'%web_exploit.replace('web','exp'))
    os.chdir("../")

def stop():
    for compose_dir in compose_dirs:
	os.chdir(compose_dir)
	os.system('docker-compose down')
	os.chdir("../")

def cmd(cmd):
    for web_container in web_containers:
        print 'resutlt for %s'%web_container
	os.system('docker exec -ti %s /bin/bash -c "%s" 2>/dev/null'%(web_container,cmd))
        print ''
    for server_container in server_containers:
        print 'resutlt for %s'%server_container
	os.system('docker exec -ti %s /bin/bash -c "%s" 2>/dev/null'%(server_container,cmd))
        print ''

def check():
    os.chdir('exp')
    for check_script in check_scripts:
        os.system('python2 %s.py'%check_script)
    os.chdir("../")

if __name__ == '__main__':
    action = sys.argv[1]
    if action == 'start':
	start()
    elif action == 'stop':
	stop()
    elif action == 'build':
        compile()
    elif action == 'init':
        init()
    elif action == 'cmd':
        cmd(sys.argv[2])
    elif action == 'exploit':
        exploit()
    elif action == 'check':
        check()
    else:
	print 'no such action'
