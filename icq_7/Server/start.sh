#!/bin/bash

# change ssh passwd
echo root:root | chpasswd

# change the privileges of files
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
cp /root/run/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
cp /root/run/my.cnf /etc/mysql//my.cnf
chmod 777 -R /usr/lib/mysql/plugin/
sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

# start services
service ssh start
service mysql start


# copy flag
cp /root/run/flag /tmp/flag

# import database
mysql -uroot -proot < /root/run/*.sql

# run the main program
#/usr/local/bin/jenkins.sh & 

# thread block
tail -f /dev/null
