# icq_rhg_20

### build and install
run the build_basic.sh under the basic_docker

### run
cd into the icq_2 directory, then docker-composer up

### check the environment
* run the exploit

```shell
python web_2.py 127.0.0.1 8001
python server_2.py 127.0.0.1 9001
```

* check

```shell
python check_2.py 127.0.0.1 8001
```


ps: You will have to wait for about 10 sec for the environments start up, then you can run the exploit/check script
