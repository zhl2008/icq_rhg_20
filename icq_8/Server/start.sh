#!/bin/bash

# change ssh passwd
echo root:root | chpasswd

# change the privileges of files

# start services
service ssh start


# copy flag
cp /root/run/flag /tmp/flag

# import database
#mysql -uroot -proot < /root/run/*.sql

# run the main program
catalina.sh run & 

# thread block
tail -f /dev/null
