#!/bin/bash

# change passwd
echo root:root | chpasswd

# clean the dirty things
/root/clean.sh

# change the privileges of files
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
chmod -R 777 /var/www/html
find /etc/apache2/ -name "*.conf" | xargs sed -i 's/${APACHE_DOCUMENT_ROOT}/\/var\/www\/public/g'

# update ssh priv
sed -i "s/#PermitRootLogin yes/PermitRootLogin yes/g" /etc/ssh/sshd_config

# start the services
service apache2 start
service ssh start

# import the database

# copy the flag
cp /root/flag /tmp/flag

# thread block
tail -f /dev/null
