#!/bin/bash

# change ssh passwd
echo root:root | chpasswd

# start services
service ssh start

# copy flag
cp /root/run/flag /tmp/flag

# run the main program
#/usr/local/bin/jenkins.sh
cd /var/;tar xvfz jenkins_home.tgz
java -Duser.home=/var/jenkins_home -Djenkins.model.Jenkins.slaveAgentPort=50000 -jar /usr/share/jenkins/jenkins.war

# thread block
tail -f /dev/null
