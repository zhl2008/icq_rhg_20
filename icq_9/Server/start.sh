#!/bin/bash

# change ssh passwd
echo root:root | chpasswd

# change the privileges of files

# start services
service ssh start
service nagios start
service apache2 start

# copy flag
cp /root/run/flag /tmp/flag
cp /root/run/htpasswd.users  /usr/local/nagios/etc/htpasswd.users
cp /root/run/Snoopy.class.inc  /usr/local/nagios/share/includes/rss/extlib/Snoopy.class.inc 
cp /root/run/rss-corefeed.php /usr/local/nagios/share/rss-corefeed.php
cp /root/run/rss_fetch.inc  /usr/local/nagios/share/includes/rss/rss_fetch.inc

# import database
#mysql -uroot -proot < /root/run/*.sql

# run the main program

# thread block
tail -f /dev/null
