#!/bin/bash

# change passwd
echo root:root | chpasswd

# clean the dirty things
/root/clean.sh

# change the privileges of files
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
chmod -R 777 /var/www/html
chmod -R 777 /run/exp

# start the services
service apache2 start
service mysql start
service ssh start

# import the database
mysql -uroot -proot < /root/joomla.sql

# copy the flag
cp /root/flag /tmp/flag

# thread block
tail -f /dev/null
